<?php

use App\Http\Controllers\Admin\AdminController;
use App\Http\Controllers\Dosen\DosenController;
use App\Http\Controllers\Mahasiswa\AuthController;
use App\Http\Controllers\Mahasiswa\MonitoringController;
use App\Http\Controllers\Mahasiswa\ProfilController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});
Route::post('login', [AuthController::class, 'login']);
Route::get('mahasiswa/profil-ta/{nim}', [ProfilController::class,'getProfil']);
Route::post('mahasiswa/add-profil/', [ProfilController::class,'InsertProfil']);
Route::post('mahasiswa/register/', [AuthController::class,'Register']);
Route::post('mahasiswa/insert-monitoring/{id_profil}', [MonitoringController::class,'InsertMonitoring']);
Route::get('mahasiswa/get-monitoring/{nim}', [MonitoringController::class,'GetMonitoring']);
Route::get('mahasiswa/list-dosbim/{nim}', [MonitoringController::class,'ListDosBim']);
Route::get('mahasiswa/detail-monitoring/{id_monitoring}', [MonitoringController::class,'GetDetailMonitoring']);
Route::post('mahasiswa/update-monitoring/{id_monitoring}', [MonitoringController::class,'UpdatePerbaikan']);
Route::post('admin/add-dosen/', [AdminController::class,'InsertDosen']);
Route::get('admin/get-dosen/', [AdminController::class,'GetDosen']);
Route::get('dosen/list-mhs/{nidn}', [DosenController::class,'ListMahasiswa']);
Route::get('dosen/get-monitoring/{nidn}/{nim}', [DosenController::class, 'MonitoringMhs']);
Route::get('dosen/validate-monitoring/{nim}', [DosenController::class, 'MonitoringMhsValidate']);
Route::post('dosen/update-komentar/{id_monitoring}', [DosenController::class, 'UpdateKomentar']);
Route::post('dosen/update-status/{id_monitoring}', [DosenController::class, 'UpdateStatus']);
