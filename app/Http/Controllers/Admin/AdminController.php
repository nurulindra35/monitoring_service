<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Dosen;

class AdminController extends Controller
{
    public function InsertDosen(Request $request)
    {
        $dosen = new Dosen();
        $dosen->nidn = $request->nidn;
        $dosen->nama = $request->nama;
        $dosen->keahlian = $request->keahlian;
        $dosen->no_hp = $request->no_hp;
        $dosen->email = $request->email;
        $dosen->password = $request->password;
        $dosen->save();

        return response()->json($dosen);
          
    }

    public function GetDosen()
    {
        $dosen =  Dosen::all();
        
       return response()->json($dosen);
    }
}
