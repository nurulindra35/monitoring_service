<?php

namespace App\Http\Controllers\Dosen;

use App\Http\Controllers\Controller;
use App\Models\Dosen;
use App\Models\Monitoring;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\Request;

class DosenController extends Controller
{
    public function ListMahasiswa($nidn)
    {
        $dosen = Dosen::where('nidn', $nidn)->with(['dataProfil.profilTA.mahasiswa'])->first();
        // dd($dosen);
        $data = [];
        foreach ($dosen->dataProfil as $key => $dataProfil) {
            $ulang = [
                'nama' => $dataProfil->profilTA->mahasiswa->nama,
                'nim' => $dataProfil->profilTA->mahasiswa->nim,
                'judul' => $dataProfil->profilTA->judul,
                'status_bimbingan' => $dataProfil->profilTA->status_bimbingan
            ];
            array_push($data, $ulang);
        }
        return response()->json($data);
    }

    public function MonitoringMhs($nidn, $nim)
    {
        $monitoring = Monitoring::whereHas('dosen', function ($dosen) use ($nidn) {
            $dosen->where('nidn', '=', $nidn);
        })->whereHas('profilTA', function (Builder $profilTA) use ($nim) {
            $profilTA->whereHas('mahasiswa', function (Builder $mahasiswa) use ($nim) {
                $mahasiswa->where('nim', '=', $nim);
            });
        })->get();
        // dd($monitoring);
        $data = [];
        foreach ($monitoring as $key => $m) {
            $ulang = [
                'id_monitoring' => $m->id_monitoring,
                'tangal_bimbingan' => $m->tanggal_bimbingan,
                'materi_bimbingan' => $m->materi_bimbingan,
                'nama_dosen' => $m->dosen->nama,
                'komentar' => $m->komentar,
                'hasil_perbaikan' => $m->hasil_perbaikan,
                'halaman' => $m->halaman,
                'status_monitoring' => $m->status_monitoring,
            ];
            array_push($data, $ulang);
        }
        return response()->json($data);
    }
    public function MonitoringMhsValidate( $nim)
    {
        $monitoring = Monitoring::whereHas('profilTA', function (Builder $profilTA) use ($nim) {
            $profilTA->whereHas('mahasiswa', function (Builder $mahasiswa) use ($nim) {
                $mahasiswa->where('nim', '=', $nim);
            });
        })->where('status_monitoring', 'ACC')->get();
        // dd($monitoring);
        $data = [];
        foreach ($monitoring as $key => $m) {
            $ulang = [
                'tangal_bimbingan' => $m->tanggal_bimbingan,
                'materi_bimbingan' => $m->materi_bimbingan,
                'nama_dosen' => $m->dosen->nama,
                'status_monitoring' => $m->status_monitoring,
            ];
            array_push($data, $ulang);
        }

        return response()->json($data);
    }

    public function UpdateKomentar(Request $request, $id_monitoring)
    {
        $komentar = $request->komentar;
        $monitoring = Monitoring::where('id_monitoring', $id_monitoring)->first();
        $monitoring->komentar = $komentar;
        $monitoring->save();

        return response()->json($monitoring);
    }
    public function UpdateStatus(Request $request, $id_monitoring)
    {
        $status_monitoring = $request->status_monitoring;

        $monitoring = Monitoring::where('id_monitoring', $id_monitoring)->first();
        $monitoring->status_monitoring = $status_monitoring;
        $monitoring->save();

        return response()->json($monitoring);
    }
}
