<?php

namespace App\Http\Controllers\Mahasiswa;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Monitoring;
use App\Models\ProfilTA;
use App\Models\Mahasiswa;

class MonitoringController extends Controller
{
    public function InsertMonitoring(Request $request, $id_profil)
    {
        $profilTA = ProfilTA::where('id_profil', $id_profil)->first();
        $monitoring = new Monitoring();
        $monitoring -> id_profil = $profilTA->id_profil;
        $monitoring -> tanggal_bimbingan = $request->tanggal_bimbingan;
        $monitoring -> materi_bimbingan = $request->materi_bimbingan;
        $monitoring -> nidn = $request->nidn;
        $monitoring->save();
        
        return response()->json($monitoring);

    }

    public function ListDosBim($nim)
    {
        $mahasiswa = Mahasiswa::where('nim', $nim)->with(['profilTA.dataProfil.dosen', 'profilTA.dataProfil.statusDosen'])->first();
        
        $data = [];
        foreach( $mahasiswa->profilTA->dataProfil as $key => $dosbim){
            $ulang = [
                'nidn'=>  $dosbim->dosen->nidn,
            'nama'=>  $dosbim->dosen->nama,
            ];
            array_push($data, $ulang);
        }
        return response()->json($data);
    }

    public function GetMonitoring($nim)
    {
        $mahasiswa = Mahasiswa::where('nim', $nim)->with(['profilTA.monitoring.dosen'])->first();
        $data = [];
        foreach($mahasiswa->profilTA->monitoring as $key => $monitoring){
            $ulang = [
                'id_monitoring'=> $monitoring->id_monitoring,
                'id_profil'=> $monitoring->id_profil,
                'tangal_bimbingan'=> $monitoring->tanggal_bimbingan,
                'materi_bimbingan'=> $monitoring->materi_bimbingan,
                'nama_dosen'=> $monitoring->dosen->nama,
                'komentar'=> $monitoring->komentar,
                'hasil_perbaikan'=> $monitoring->hasil_perbaikan,
                'halaman'=> $monitoring->halaman,
                'status_monitoring'=> $monitoring->status_monitoring,
            ];
            array_push($data, $ulang);
        }
        return response()->json($data);
    }

    public function GetDetailMonitoring($id_monitoring)
    { 
        $monitoring = Monitoring::where('id_monitoring',$id_monitoring)->first();
        $data = [
            'tanggal_bimbingan'=> $monitoring->tanggal_bimbingan,
            
        ];
        return response()->json($data);
    }

    public function UpdatePerbaikan(Request $request,$id_monitoring)
    {
        
        $hasil_perbaikan = $request->hasil_perbaikan;
        $halaman = $request->halaman;
        $monitoring = Monitoring::where('id_monitoring',$id_monitoring)->first();
        $monitoring->hasil_perbaikan = $hasil_perbaikan;
        $monitoring->halaman = $halaman;
        $monitoring->save();

        return response()->json($monitoring);
    }

}
