<?php

namespace App\Http\Controllers\Mahasiswa;

use App\Http\Controllers\Controller;
use App\Models\DataProfil;
use App\Models\Mahasiswa;
use App\Models\ProfilTA;
use Illuminate\Http\Request;

class ProfilController extends Controller
{
    //untik get profil
    public function getProfil($nim)
    {
        $mahasiswa = Mahasiswa::where('nim', $nim)->with(['profilTA.dataProfil.dosen', 'profilTA.dataProfil.statusDosen'])->first();

        // dd($mahasiswa);
        $data = [
            'id_profil' => $mahasiswa->profilTA->id_profil,
            'judul' => $mahasiswa->profilTA->judul,
            //
            $mahasiswa->profilTA->dataProfil[0]->statusDosen->status_dosen => $mahasiswa->profilTA->dataProfil[0]->dosen->nama,
            $mahasiswa->profilTA->dataProfil[1]->statusDosen->status_dosen => $mahasiswa->profilTA->dataProfil[1]->dosen->nama,
            'tanggal_mulai' => $mahasiswa->profilTA->tanggal_mulai,
            'tanggal_selesai' => $mahasiswa->profilTA->tanggal_selesai,
            'status_bimbingan' => $mahasiswa->profilTA->status_bimbingan,
        ];
        return response()->json($data);

    }

    public function InsertProfil(Request $request)
    {
        //$mahasiswa = Mahasiswa::where('nim', $nim)->first();
        //return($mahasiswa->nim);
        $profilTA = new ProfilTA();
        $profilTA->nim = $request->nim;
        $profilTA->judul = $request->judul;
        $profilTA->tanggal_mulai = $request->tanggal_mulai;
        $profilTA->tanggal_selesai = $request->tanggal_selesai;
        $profilTA->status_bimbingan = 'Baru';
        $profilTA->save();


        $dosen1 = new DataProfil();
        $dosen1->nidn = $request->nidn1;
        $dosen1->id_profil = $profilTA->id_profil;
        $dosen1->id_status_dosen = $request->status1;
        $dosen1->save();

        
        $dosen1 = new DataProfil();
        $dosen1->nidn = $request->nidn2;
        $dosen1->id_profil = $profilTA->id_profil;
        $dosen1->id_status_dosen = $request->status2;
        $dosen1->save();

        return response()->json($profilTA);
    }
}
