<?php

namespace App\Http\Controllers\Mahasiswa;

use App\Http\Controllers\Controller;
use App\Models\Admin;
use App\Models\Dosen;
use App\Models\Mahasiswa;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

class AuthController extends Controller
{
    public function Register(Request $request)
    {
        $nim = $request->nim;
        $mhs = Mahasiswa::where('nim', $nim)->first();
        if ($mhs) {
            // your code...
            return response()->json([
                'success' => false,
                'message' => 'Mahasiswa telah terdaftar',
            ], 400);
        } else {
            $mahasiswa = new Mahasiswa();
            $mahasiswa->nim = $request->nim;
            $mahasiswa->nama = $request->nama;
            $mahasiswa->email = $request->email;
            $mahasiswa->password = Hash::make($request->password);
            $mahasiswa->no_hp = $request->no_hp;
            // if ($request->hasFile('image')) {  
            //     $path = $request->file('image')->store('images');
            //     $mahasiswa->image = $path;
            //    }
            $mahasiswa->save();
        }

        return response()->json($mahasiswa);
    }

    public function Login(Request $request)
    {
        $email = $request->email;
        $password = $request->password;
        //$status = $request->status;

        $mahasiswa = Mahasiswa::where('email', $email)->first();
        
        if (!$mahasiswa) {
            $dosen = Dosen::where('email', $email)->first();
            
            if (!$dosen) {
                $admin = Admin::where('email', $email)->first();
                
                if (!$admin) {
                    return response()->json([
                        'success' => false,
                        'message' => 'Akun Tidak Ditemukan',
                    ], 401);
                }
                $admin['role'] = "Admin";
                if (Hash::check($password, $admin->password)) {
                    unset($admin['password']);
                    return response($admin, 200);
                }
               
                return response()->json([
                    'success' => false,
                    'message' => 'Invalid Email or Password',
                ], 401);
            }
            $dosen['role'] = "Dosen";
            if (Hash::check($password, $dosen->password)) {
                unset($dosen['password']);
                return response($dosen, 200);
            }
           
            return response()->json([
                'success' => false,
                'message' => 'Invalid Email or Password',
            ], 401);
        }
        $mahasiswa['role'] = "Mahasiswa";
        if (Hash::check($password, $mahasiswa->password)) {
            unset($mahasiswa['password']);
            return response($mahasiswa, 200);
        }
        
        return response()->json([

            'success' => false,
            'message' => 'Invalid Email or Password',
        ], 401);

    }
}
