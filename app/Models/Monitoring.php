<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * @property int $id_monitoring
 * @property int $id_profil
 * @property string $tanggal_bimbingan
 * @property string $materi_bimbingan
 * @property string $komentar
 * @property string $hasil_perbaikan
 * @property string $halaman
 * @property string $status_monitoring
 * @property ProfilTum $profilTum
 */
class Monitoring extends Model
{
    /**
     * The table associated with the model.
     * 
     * @var string
     */
    protected $table = 'monitoring';
    public $timestamps = false;

    /**
     * The primary key for the model.
     * 
     * @var string
     */
    protected $primaryKey = 'id_monitoring';

    /**
     * @var array
     */
    protected $fillable = ['id_profil', 'tanggal_bimbingan', 'materi_bimbingan', 'komentar', 'hasil_perbaikan', 'halaman', 'status_monitoring'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function profilTA()
    {
        return $this->belongsTo('App\Models\ProfilTA', 'id_profil', 'id_profil');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function dosen()
    {
        return $this->belongsTo('App\Models\Dosen', 'nidn', 'nidn');
    }
}
