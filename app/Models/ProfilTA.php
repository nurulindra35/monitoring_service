<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * @property int $id_profil
 * @property string $nim
 * @property string $judul
 * @property string $tanggal_mulai
 * @property string $tanggal_selesai
 * @property string $status_bimbingan
 * @property Mahasiswa $mahasiswa
 * @property DataProfil[] $dataProfil
 * @property Monitoring $monitoring
 */
class ProfilTA extends Model
{
    /**
     * The table associated with the model.
     * 
     * @var string
     */
    protected $table = 'profil_ta';
    public $timestamps = false; 

    /**
     * The primary key for the model.
     * 
     * @var string
     */
    protected $primaryKey = 'id_profil';

    /**
     * @var array
     */
    protected $fillable = ['nim', 'judul', 'tanggal_mulai', 'tanggal_selesai', 'status_bimbingan'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function mahasiswa()
    {
        return $this->belongsTo('App\Models\Mahasiswa', 'nim', 'nim');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function dataProfil()
    {
        return $this->hasMany('App\Models\DataProfil', 'id_profil', 'id_profil');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function monitoring()
    {
        return $this->hasMany('App\Models\Monitoring', 'id_profil', 'id_profil');
    }
}
