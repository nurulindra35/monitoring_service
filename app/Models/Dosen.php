<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * @property string $nidn
 * @property string $nama
 * @property string $keahlian
 * @property string $no_hp
 * @property string $email
 * @property string $password
 * @property DataProfil[] $dataProfils
 */
class Dosen extends Model
{
    /**
     * The table associated with the model.
     * 
     * @var string
     */
    protected $table = 'dosen';
    public $timestamps = false;

    /**
     * The primary key for the model.
     * 
     * @var string
     */
    protected $primaryKey = 'nidn';

    /**
     * The "type" of the auto-incrementing ID.
     * 
     * @var string
     */
    protected $keyType = 'string';

    /**
     * Indicates if the IDs are auto-incrementing.
     * 
     * @var bool
     */
    public $incrementing = false;

    /**
     * @var array
     */
    protected $fillable = ['nama', 'keahlian', 'no_hp', 'email', 'password'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function dataProfil()
    {
        return $this->hasMany('App\Models\DataProfil', 'nidn', 'nidn');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function monitoring()
    {
        return $this->hasMany('App\Models\Monitoring', 'nidn', 'nidn');
    }
}
