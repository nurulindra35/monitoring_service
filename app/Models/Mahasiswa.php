<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * @property string $nim
 * @property string $nama
 * @property string $email
 * @property string $password
 * @property string $no_hp
 * @property ProfilTum $profilTum
 */
class Mahasiswa extends Model
{
    /**
     * The table associated with the model.
     * 
     * @var string
     */
    protected $table = 'mahasiswa';
    public $timestamps = false;

    /**
     * The primary key for the model.
     * 
     * @var string
     */
    protected $primaryKey = 'nim';

    /**
     * The "type" of the auto-incrementing ID.
     * 
     * @var string
     */
    protected $keyType = 'string';

    /**
     * Indicates if the IDs are auto-incrementing.
     * 
     * @var bool
     */
    public $incrementing = false;

    /**
     * @var array
     */
    protected $fillable = ['nama', 'email', 'password', 'no_hp'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function profilTA()
    {
        return $this->hasOne('App\Models\ProfilTA', 'nim', 'nim');
    }
}
