<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * @property int $id_status
 * @property string $status_dosen
 * @property DataProfil[] $dataProfils
 */
class StatusDosen extends Model
{
    /**
     * The table associated with the model.
     * 
     * @var string
     */
    protected $table = 'status_dosen';
    public $timestamps = false;

    /**
     * The primary key for the model.
     * 
     * @var string
     */
    protected $primaryKey = 'id_status';

    /**
     * @var array
     */
    protected $fillable = ['status_dosen'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function dataProfil()
    {
        return $this->hasMany('App\Models\DataProfil', 'id_status_dosen', 'id_status');
    }
}
