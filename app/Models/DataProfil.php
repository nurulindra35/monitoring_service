<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * @property int $id
 * @property string $nidn
 * @property int $id_profil
 * @property int $id_status_dosen
 * @property Dosen $dosen
 * @property ProfilTum $profilTum
 * @property StatusDosen $statusDosen
 */
class DataProfil extends Model
{
    /**
     * The table associated with the model.
     * 
     * @var string
     */
    protected $table = 'data_profil';
    public $timestamps = false;

    protected $primaryKey = 'id';



    /**
     * @var array
     */
    protected $fillable = ['nidn', 'id_profil', 'id_status_dosen'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function dosen()
    {
        return $this->belongsTo('App\Models\Dosen', 'nidn', 'nidn');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function profilTA()
    {
        return $this->belongsTo('App\Models\ProfilTA', 'id_profil', 'id_profil');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function statusDosen()
    {
        return $this->belongsTo('App\Models\StatusDosen', 'id_status_dosen', 'id_status');
    }
}
